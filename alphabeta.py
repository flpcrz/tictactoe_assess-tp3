# Retorna a configuração do tabuleiro e sua utilidade
def alphabeta(game_state, alpha=-2, beta=2, our_turn=True):
    # SE nó é um nó terminal OU profundidade = 0
    if game_state.is_gameover():
        return game_state.score(), None
    if our_turn:
        # maximizador é TRUE | utilidade máxima = -Infinit
        score = -2
        # PARA CADA filho do nó | Utilidade
        for move in game_state.get_possible_moves():
            child = game_state.get_next_state(move, True)
            # Escolher a maior dentre as perdas causadas pelo minimizador
            temp_max, _ = alphabeta(child, alpha, beta, False)
            # SE a utilidade > utilidade máxima
            if temp_max > score:
                score = temp_max
                best_move = move
            alpha = max(alpha, score)
            # melhor opção do maximizador no caminho para a raiz
            if beta <= alpha:
                break
        # Utilidade máxima e jogada com a maior utilidade
        return score, best_move
    else:
        # maximizador é FALSE | utilidade máxima = +Infinit 
        score = 2
        # PARA CADA filho do nó | Utilidade
        for move in game_state.get_possible_moves():
            child = game_state.get_next_state(move, False)
            # Escolher o menor dentre os ganhos causadas pelo maximizador
            temp_min, _ = alphabeta(child, alpha, beta, True)
            # SE a utilidade < utilidade mínima
            if temp_min < score:
                score = temp_min
                best_move = move
            beta = min(beta, score)
            # melhor opção do minimizador no caminho para a raiz
            if beta <= alpha:
                break
        # Utilidade mínima e jogada com a menor utilidade    
        return score, best_move

def negamax(game_state, alpha=-2, beta=2, our_turn=True):
    # SE nó é um nó terminal OU profundidade = 0
    if game_state.is_gameover():
        return game_state.score(), None 

    score = -2

    for move in game_state.get_possible_moves():        
        child = game_state.get_next_state(move, True) 

        # Escolher alternadamente entre os turnos
        temp_max, _ =  negamax(child, -alpha, -beta, not our_turn)    
        
        # SE a utilidade > utilidade máxima        
        if temp_max > score:
                score = temp_max
                best_move = move

        alpha = max(alpha, score)
        # melhor opção no caminho para a raiz
        if alpha >= beta:
            break
    # Utilidade máxima
    return score, best_move